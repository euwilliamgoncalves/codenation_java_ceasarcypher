import urllib.request
import json
import hashlib
from hashlib import sha1
from pip._internal.utils import encoding
from pip._vendor import requests

url = 'https://api.codenation.dev/v1/challenge/dev-ps/generate-data?token=b1673fd8deedc61a0d8006e3bfb45653ff0abc4f'

req = urllib.request.Request(url)

r = urllib.request.urlopen(req).read()
data = json.loads(r.decode('utf-8'))
cifrado = data['cifrado']

print ('\n Mensagem cifrada: ' + cifrado + '\n')

letras = 'abcdefghijklmnopqrstuvwxyz'

for key in range(len(letras)):
    traducao = ''
    for letra in cifrado:
        if letra in letras:
            num = letras.find(letra)
            num = num - 11
            traducao = traducao + letras[num]
        else:
            traducao = traducao + letra

print('\n Mensagem decifrada: ' + traducao + '\n')

data['decifrado'] = traducao

resumo = hashlib.sha1(data['decifrado'].encode()).hexdigest()

print ('\n Resumo criptográfico: ' + resumo + '\n')

data['resumo_criptografico'] = resumo

answer = json.dumps(data)

text_file = open("answer.json", "w")
text_file.write(answer)
text_file.close()

print (answer)

#url = "https://api.codenation.dev/v1/challenge/dev-ps/submit-solution?token=b1673fd8deedc61a0d8006e3bfb45653ff0abc4f"