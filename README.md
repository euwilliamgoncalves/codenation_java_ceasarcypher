# codenation_java_ceasarcypher
Aplicação criada para o desafio do AceleraDev Java de 2020, da Codenation.

codenation.py 
- request do arquivo JSON através da API
- carrega dados do JSON em uma array
- descriptografa a mensagem
- gera resumo criptográfico
- preenche o array com os novos dados
- gera arquivo 'answer.json'

envia.html
- envia o arquivo 'answer.json' através do método POST no formato 'multipart/form-data'
